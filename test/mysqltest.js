const mysql = require('mysql');

const connection = mysql.createConnection({
    host     : '115.71.233.22',
    user     : 'testuser',
    password : 'testuser!@#',
    database : 'testdb'
});
 
connection.connect();   //실제로 이 connect도 다른 process로 넘김

connection.beginTransaction(err2 => {
    connection.query('update User set lastlogin=now() where uid=?', ['user2'], function (error, results, fields) {
        if (error) throw error;
        console.log('The Update', results.affectedRows);
        
        //여기에 update, insert다 된다. 
        connection.query('select * from User where uid=?', ['user2'], function (error, results, fields) {
            if (error) throw error;
            console.log('The First User is: ', results[0]);
    
            connection.query('delete', (error) => {
                if(error) throw error;
                
                connection.end();   // = connection close, end를 항상 해줘야 한다. 
            })
            
        });
    });
});


 
