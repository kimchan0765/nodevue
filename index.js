const express = require('express'), //웹서버
      app = express();  //app은 express를 init(start아님)

const Pool = require('./pool'),
      Mydb = require('./mydb');

const testJson = require('./test/test.json');

const pool = new Pool();

app.use(express.static('public'));
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');  //view엔진은 ejs를 쓰겠다고 선언
app.engine('html', require('ejs').renderFile);

// // methoe   request,response
// app.get('/', (req, res) => {
//     // res.send("Hello NodeJS!!");
//     // res.json(testJson);
//     res.render('index', {name: 'KimChan'})
// });

//           uri에서 변수값을 가져오겠다. 
app.get('/test/:email', (req, res) => {
    testJson.email = req.params.email;  // cf. req.body, req.query
    testJson.aaa = req.query.aaa;
    res.json(testJson);
});

app.get('/dbtest/:user', (req, res) => {
    let user = req.params.user;
    let mydb = new Mydb(pool);
    mydb.execute(conn => {
        conn.query("select * from User where uid = ?", [user], (err, ret) => {
            res.json(ret);
        });
    });
});


const server = app.listen(7000, function(){
    console.log("Express's started on port 7000");
});
